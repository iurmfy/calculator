package com.iurmfy.calculator;

import javax.swing.*;
import java.awt.*;

// 计算器窗体
public class CalFrame extends JFrame
{
    private static final int WIDTH = 300;
    private static final int HEIGHT = 400;

    // 用于数据显示
    private static final JLabel label = new JLabel("  ");

    // 添加按钮
    private static void addButton(String name, JFrame frame, GridBagLayout layout, GridBagConstraints constraints)
    {
        JButton button = new JButton(name);
        button.addActionListener(new CalButtonListener());
        layout.setConstraints(button, constraints);
        frame.add(button);
    }

    // 设置窗体布局
    public CalFrame()
    {
        GridBagLayout bagLayout = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();

        setLayout(bagLayout);

        constraints.fill = GridBagConstraints.BOTH;

        constraints.weightx=0.5;
        constraints.weighty=0.4;

        constraints.gridwidth = GridBagConstraints.REMAINDER;


        label.setFont(new Font(Font.DIALOG,Font.PLAIN,25));

        bagLayout.setConstraints(label, constraints);
        add(label);

        constraints.weightx=0.5;
        constraints.weighty=0.1;

        constraints.gridwidth=1;
        addButton("AC",this,bagLayout,constraints);
        addButton("+/-",this,bagLayout,constraints);
        addButton("%",this,bagLayout,constraints);
        constraints.gridwidth=GridBagConstraints.REMAINDER;
        addButton("/",this,bagLayout,constraints);

        constraints.gridwidth=1;
        addButton("7",this,bagLayout,constraints);
        addButton("8",this,bagLayout,constraints);
        addButton("9",this,bagLayout,constraints);
        constraints.gridwidth=GridBagConstraints.REMAINDER;
        addButton("*",this,bagLayout,constraints);

        constraints.gridwidth=1;    //重新设置gridwidth的值
        addButton("4",this,bagLayout,constraints);
        addButton("5",this,bagLayout,constraints);
        addButton("6",this,bagLayout,constraints);
        constraints.gridwidth=GridBagConstraints.REMAINDER;
        addButton("-",this,bagLayout,constraints);

        constraints.gridwidth=1;
        addButton("1",this,bagLayout,constraints);
        addButton("2",this,bagLayout,constraints);
        addButton("3",this,bagLayout,constraints);
        constraints.gridwidth=GridBagConstraints.REMAINDER;
        addButton("+",this,bagLayout,constraints);

        constraints.gridwidth=2;
        addButton("0",this,bagLayout,constraints);
        addButton(".",this,bagLayout,constraints);
        constraints.gridwidth=GridBagConstraints.REMAINDER;
        addButton("=",this,bagLayout,constraints);

        setSize(WIDTH, HEIGHT);
        CalFrame.setLabel("0");
        // 确保窗口在屏幕中央
        setLocationRelativeTo(null);
        setTitle("Calculator");
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    // 设置标签内容
    public static void setLabel(String text)
    {
        label.setText(text);
    }

    // 得到标签内容
    public static String getLabel()
    {
        return label.getText();
    }
}
