package com.iurmfy.calculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// 按钮事件
public class CalButtonListener implements ActionListener
{
    private static double preValue = 0;
    //private static double thisValue = 0;

    // 操作模式
    private static byte operation;

    //private static String calShowContent = "";

    // 显示小数
    //private static boolean isShowFloat;

    //private static String calContent = ""; // 显示在label上的内容

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String content = e.getActionCommand();

        int chr = content.charAt(0);

        switch (chr)
        {
            case 65 -> // AC
            {
                CalFrame.setLabel("0");
                preValue = 0;
            }
            case 43 ->
            {
                if (content.equals("+/-")) // 取负数
                {
                    String str = CalFrame.getLabel();
                    str = "-" + str;
                    CalFrame.setLabel(str);
                }
                else // 加法
                {
                    operation = 1;
                    preValue = Double.parseDouble(CalFrame.getLabel());
                    CalFrame.setLabel("0");
                }
            }

            case 47 ->
            { // 除法
                operation = 2;
                preValue = Double.parseDouble(CalFrame.getLabel());
                CalFrame.setLabel("0");
            }

            case 42 -> // 乘法
                    operation = 3;

            case 45 -> // 减法
                    operation = 4;

            case 37 -> // 百分号
            {
                double val = Double.parseDouble(CalFrame.getLabel());
                val *= 0.01;
                CalFrame.setLabel(String.valueOf(val));
            }

            case 61 -> // 等于号
            {
                switch (operation)
                {
                    case 1-> // +
                    {
                        double thisValue = Double.parseDouble(CalFrame.getLabel());
                        thisValue += preValue;
                        CalFrame.setLabel(String.valueOf(thisValue));

                        preValue = thisValue;
                    }

                    case 2-> // /
                    {
                        double thisValue = Double.parseDouble(CalFrame.getLabel());
                        thisValue /= preValue;
                        CalFrame.setLabel(String.valueOf(thisValue));

                        preValue = thisValue;
                    }

                    case 3-> // *
                    {
                        double thisValue = Double.parseDouble(CalFrame.getLabel());
                        thisValue *= preValue;
                        CalFrame.setLabel(String.valueOf(thisValue));

                        preValue = thisValue;
                    }

                    case 4 -> // -
                    {
                        double thisValue = Double.parseDouble(CalFrame.getLabel());
                        thisValue -= preValue;
                        CalFrame.setLabel(String.valueOf(thisValue));

                        preValue = thisValue;
                    }

                }
            }

            default -> // 数字 小数点
            {
                //calShowContent = calShowContent + content;
                String str = CalFrame.getLabel();
                str += content;
                CalFrame.setLabel(str);
            }
        }
    }
}
